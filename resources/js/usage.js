jQuery(document).ready(function ($) {
    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 900);
    });
});
$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '<Ant',
    nextText: 'Sig>',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'yy/mm/dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
    $("#datein").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        onClose: function (selectedDate) {
            $("#dateout").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#dateout").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        onClose: function (selectedDate) {
            $("#datein").datepicker("option", "maxDate", selectedDate);
        }
    });
});
$(function () {
    var dialog, form, dialogF,
            // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
            emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
            name = $("#nombre"),
            ap = $("#ap"),
            ap1 = $("#ap1"),
            tel = $("#tel"),
            email = $("#mail"),
            nac = $("#nac"),
            tip = $("#tip option:selected").val(),
            checkin = $("#datein"),
            checkout = $("#dateout"),
            noti = $("#not"),
            allFields = $([]).add(name).add(email).add(nac).add(ap).add(ap1).add(tel)
            .add(tip).add(checkin).add(checkout).add(noti),
            tips = $(".validateTips");
    function updateTips(t) {
        tips
                .text(t)
                .addClass("ui-state-highlight");
        setTimeout(function () {
            tips.removeClass("ui-state-highlight", 1500);
        }, 500);
    }

    function checkLength(o, n, min, max) {
        if (o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            updateTips("El tamaño minimo " + n + " es de " +
                    min + " y maximo de " + max + ".");
            return false;
        } else {
            return true;
        }
    }

    function checkRegexp(o, regexp, n) {
        if (!(regexp.test(o.val()))) {
            o.addClass("ui-state-error");
            updateTips(n);
            return false;
        } else {
            return true;
        }
    }

    function sndReg() {

        var valid = true;
        allFields.removeClass("ui-state-error");
        valid = valid && checkLength(name, "nombre", 3, 16);
        valid = valid && checkLength(ap, "apellido paterno", 3, 16);
        valid = valid && checkLength(ap1, "apellido materno", 3, 16);
        valid = valid && checkLength(tel, "telefono", 10, 13);
        valid = valid && checkLength(nac, "nacionalidad", 3, 16);
        valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
        valid = valid && checkRegexp(ap, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
        valid = valid && checkRegexp(ap1, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
        valid = valid && checkRegexp(tel, /^([0-9\s])+$/i, "Campo consiste en caracteres  0-9.");
        valid = valid && checkRegexp(nac, /^[a-z]([a-z\s])+$/i, "Campo consiste en caracteres a-z espacios y debe comenzar con una letra.");
//            valid = valid && checkRegexp(email, emailRegex, "eg. contacto@proyectoucq.com");
        if (valid) {
//                snd(name, ap, ap1, tel, email, nac, tip, checkin, checkout, noti);
            var data = {
                "nombre": name.val(),
                "ap": ap.val(),
                "ap1": ap1.val(),
                "tel": tel.val(),
                "email": email.val(),
                "nac": nac.val(),
                "tip": tip,
                "checkin": checkin.val(),
                "checkout": checkout.val(),
                "note": noti.val()
            };
            console.log(data);
            $.ajax({
                type: 'POST',
                url: 'welcome/registry',
                data: {
                    data: JSON.stringify(data),
                },
                success: function (data) {
                    if (data.stat === 1) {
                        alert("solicitud enviada");
                    } else {
                        alert("lo sentimos porfavor contactanos al numero x-xx-xx-xx");
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("lo sentimos porfavor contactanos al numero x-xx-xx-xx");
                },
                dataType: 'json'
            });
            dialog.dialog("close");
        }
        return valid;
    }

    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        height: 600,
        width: 450,
        modal: true,
        buttons: {
            "Envia registro": sndReg,
            Cancel: function () {
                dialog.dialog("close");
            }
        },
        close: function () {
            form[ 0 ].reset();
            allFields.removeClass("ui-state-error");
        }
    });
    form = dialog.find("form").on("submit", function (event) {
        event.preventDefault();
        sndReg();
    });
    $("#create-user").button().on("click", function () {
        dialog.dialog("open");
    });
});
function contact() {
    document.getElementById('filldetails').onsubmit = function () {
        var data = {
            nombre: $('#name').val(),
            phone: $('#phone').val(),
            mail: $('#email').val(),
            message: $('#msg').val()
        };
        $.ajax({
            type: 'POST',
            url: 'welcome/contact',
            data: {
                data: JSON.stringify(data),
            },
            success: function (data) {
                if (data.stat === 1) {
                    alert("gracias por sus comentarios");
                }
            }, error: function () {
                alert("lo sentimos porfavor contactanos al numero x-xx-xx-xx");
            },
            dataType: 'json'
        });
        return false;
    };

}