<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>jQuery UI Dialog - Modal form</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <style>
            label, input { display:block; }
            input.text { margin-bottom:12px; width:95%; padding: .4em; }
            fieldset { padding:0; border:0; margin-top:25px; }
            h1 { font-size: 1.2em; margin: .6em 0; }
            div#users-contain { width: 350px; margin: 20px 0; }
            div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
            div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
            .ui-dialog .ui-state-error { padding: .3em; }
            .validateTips { border: 1px solid transparent; padding: 0.3em; }
        </style>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 900);
                });
            });
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $(function () {
                $("#datein").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3,
                    onClose: function (selectedDate) {
                        $("#dateout").datepicker("option", "minDate", selectedDate);
                    }
                });
                $("#dateout").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3,
                    onClose: function (selectedDate) {
                        $("#datein").datepicker("option", "maxDate", selectedDate);
                    }
                });
            });
            $(function () {
                var dialog, form,
                        // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
                        emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
                        name = $("#nombre"),
                        ap = $("#ap"),
                        ap1 = $("#ap1"),
                        tel = $("#tel"),
                        email = $("#email"),
                        nac = $("#nac"),
                        tip = $("#tip"),
                        checkin = $("#datein"),
                        checkout = $("#dateout"),
                        noti = $("#not"),
                        allFields = $([]).add(name).add(email).add(nac).add(ap).add(ap1).add(tel)
                                .add(tip).add(checkin).add(checkout).add(noti),
                        tips = $(".validateTips");
                function updateTips(t) {
                    tips
                            .text(t)
                            .addClass("ui-state-highlight");
                    setTimeout(function () {
                        tips.removeClass("ui-state-highlight", 1500);
                    }, 500);
                }

                function checkLength(o, n, min, max) {
                    if (o.val().length > max || o.val().length < min) {
                        o.addClass("ui-state-error");
                        updateTips("El tamaño minimo " + n + " es de " +
                                min + " y maximo de " + max + ".");
                        return false;
                    } else {
                        return true;
                    }
                }

                function checkRegexp(o, regexp, n) {
                    if (!(regexp.test(o.val()))) {
                        o.addClass("ui-state-error");
                        updateTips(n);
                        return false;
                    } else {
                        return true;
                    }
                }

                function sndReg() {

                    var valid = true;
                    allFields.removeClass("ui-state-error");
                    valid = valid && checkLength(name, "nombre", 3, 16);
                    valid = valid && checkLength(ap, "apellido paterno", 3, 16);
                    valid = valid && checkLength(ap1, "apellido materno", 3, 16);
                    valid = valid && checkLength(tel, "telefono", 10, 13);
                    valid = valid && checkLength(nac, "nacionalidad", 3, 16);
                    valid = valid && checkLength(email, "email", 6, 80);
                    valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
                    valid = valid && checkRegexp(ap, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
                    valid = valid && checkRegexp(ap1, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
                    valid = valid && checkRegexp(tel, /^([0-9\s])+$/i, "Campo consiste en caracteres  0-9.");
                    valid = valid && checkRegexp(nac, /^[a-z]([a-z\s])+$/i, "Campo consiste en caracteres a-z espacios y debe comenzar con una letra.");
                    valid = valid && checkRegexp(email, emailRegex, "eg. contacto@proyectoucq.com");
                    if (valid) {
                        $.ajax({
                            type: 'POST',
//                          url: '<? // base_url('welcome/registry') ?>',
                            data: {
                                nombre: name,
                                ap: ap,
                                ap1: ap1,
                                tel: tel,
                                email: email,
                                nac: nac,
                                tip: tip,
                                checkin: checkin,
                                checkout: checkout,
                                note: noti  
                            },
                            success: function (data) {
                                if (data.stat === 1) {
                                    alert("solicitud enviada");
                                }
                            }, error: function () {
                                alert("lo sentimos porfavor contactanos al numero x-xx-xx-xx");
                            },
                            dataType: 'json'
                        });
                        dialog.dialog("close");
                    }
                    return valid;
                }

                dialog = $("#dialog-form").dialog({
                    autoOpen: false,
                    height: 600,
                    width: 450,
                    modal: true,
                    buttons: {
                        "Envia registro": sndReg,
                        Cancel: function () {
                            dialog.dialog("close");
                        }
                    },
                    close: function () {
                        form[ 0 ].reset();
                        allFields.removeClass("ui-state-error");
                    }
                });
                form = dialog.find("form").on("submit", function (event) {
                    event.preventDefault();
                    addUser();
                });
                $("#create-user").button().on("click", function () {
                    dialog.dialog("open");
                });
            });
            function contact() {
                $.ajax({
                    type: 'POST',
//                          url: '<? // base_url('welcome/contact') ?>',
                    data: {
                        nombre: $('#name').val(),
                        phone: $('#tel').val(),
                        mail: $('#email').val(),
                        message: $('#msg').val()
                    },
                    success: function (data) {
                        if (data.stat === 1) {
                            alert("solicitud enviada");
                        }
                    }, error: function () {
                        alert("lo sentimos porfavor contactanos al numero x-xx-xx-xx");
                    },
                    dataType: 'json'
                });
            }
        </script>
    </head>
    <body>

        <div id="dialog-form" title="Registro">
            <p class="validateTips">Todos los campos son requeridos</p>

            <form>
                <fieldset>
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" value="" class="text ui-widget-content ui-corner-all">
                    <label for="ap">Apellido paterno</label>
                    <input type="text" name="ap" id="ap" value="" class="text ui-widget-content ui-corner-all">
                    <label for="ap1">Apellido materno</label>
                    <input type="text" name="ap1" id="ap1" value="" class="text ui-widget-content ui-corner-all">
                    <label for="tel">Telefono</label>
                    <input type="tel" name="tel" id="tel" value="" class="text ui-widget-content ui-corner-all">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" value="" class="text ui-widget-content ui-corner-all">
                    <label for="nac">Nacionalidad</label>
                    <input type="text" name="nac" id="nac" value="" class="text ui-widget-content ui-corner-all">
                    <!--$checkin, $checkout, $notes-->
                    <label for="tip">Tipo de habitacion</label>
                    <select name="tip" id="tip" class= "ui-widget-content ui-corner-all">
                        <option value="SENCILLA">SENCILLA</option>
                        <option value="DOBLE">DOBLE</option>
                        <option value="TRIPLE">TRIPLE</option>
                        <option value="CUADRUPLE">CUADRUPLE</option>
                        <option value="SUITE">SUITE</option>
                    </select>
                    <label for="datein">Fecha de inicio</label>
                    <input id="datein" name="datein" type="text" class="text ui-widget-content ui-corner-all">
                    <label for="dateout">Fecha de final</label>
                    <input id="dateout" name="dateout" type="text" class="text ui-widget-content ui-corner-all">

                    <label for="not">Notas</label>
                    <input type="text" name="not" id="not" value="" class="text ui-widget-content ui-corner-all">
                    <!-- Allow form submission with keyboard without duplicating the dialog button -->
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                </fieldset>
            </form>
        </div>


        //codigo boton registro
        <button id="create-user">Echale candela!</button>


    </body>
</html>
