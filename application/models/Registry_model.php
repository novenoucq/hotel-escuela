<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Registry_model extends CI_Model {

    public function checkDisp($checkin, $checkout) {
        $sql = "SELECT * " .
                "FROM  `reservation` " .
                "WHERE checkin = $checkin AND checkout = $checkout " .
                "AND status = 'CANCELLED' ";
        if ($this->db->query($sql)) {
            return TRUE;
        }
        return FALSE;
    }

    public function insGuest($guest, $reg) {
        $data = array(
            "name" => strtoupper($guest["name"]),
            "last_name" => strtoupper($guest["ap"]),
            "last_name2" => strtoupper($guest["ap1"]),
            "phone" => strtoupper($guest["tel"]),
            "email" => strtoupper($guest["email"]),
            "nationality" => strtoupper($guest["nac"])
        );
        $result = $this->db->query('SELECT idguest FROM guest '
                . 'WHERE name = "' . $guest["name"] . '" AND last_name = "' . $guest["ap"] . '" AND '
                . 'last_name2 = "' . $guest["ap1"] . '" AND phone = "' . $guest["tel"] . '"');
        if ($result->num_rows() == 0) {
            if ($this->db->insert('guest', $data)) {
                $id = $this->db->insert_id();
                return $this->insRes($id, $reg);
            } else {
                return FALSE;
            }
        } else {
            $result = $result->row();
            return $this->insRes($result->idguest, $reg);
        }
    }

    private function insRes($idguest, $reg) {
        /*
         * reservation
         * (`idguest`, `idbedroom`, `checkin`, `checkout`, `notes`, `status`)
         */
            $data = array(
                'idguest' => $idguest,
                'idbedroom' => $reg["idbedroom"],
                'checkin' => $reg["checkin"],
                'checkout' => $reg["checkout"],
                'notes' => $reg["note"] 
            );
            return $this->db->insert('reservation', $data);
    }

    public function getFreeBedrooms($startdate, $enddate, $type){
        $row_array = array();
       $sql ="SELECT * FROM bedrooms
            WHERE bedrooms.idbedrooms NOT IN (
                SELECT registrys.idbedroom
                FROM registrys
                WHERE (
                    (registrys.checkin BETWEEN	'$startdate' AND '$enddate') OR
                    (registrys.checkout BETWEEN	'$startdate' AND '$enddate') OR
                    (registrys.checkin <= '$startdate' AND registrys.checkout >= '$enddate') 
                ) AND registrys.status !='CHECKOUT'
                ) AND idbedrooms NOT IN (
                SELECT reservation.idbedroom
                FROM reservation
                WHERE (
                        (reservation.checkin BETWEEN '$startdate' AND '$enddate') OR
                        (reservation.checkout BETWEEN '$startdate' AND '$enddate') OR
                        (reservation.checkin <= '$startdate' AND reservation.checkout >= '$enddate') 
                    ) AND reservation.status !='CANCELLED'
            ) AND (bedrooms.type = '$type' OR '$type' = '0');";
        $rows = $this->db->query($sql);
         foreach ($rows->result() as $row) {
             $row_array[] =array( 
                        "idbedrooms" => $row->idbedrooms,
                        "number" => $row->number,
                        "description"=>$row->description,
                        "price" => $row->price
                     );
         }
         $data["data"]=$row_array;
        return $data;
    }

    public function insCon($contact) {
        $data = array(
            "name" => $contact["name"],
            "phone" => $contact["phone"],
            "email" => $contact["mail"],
            "message" => $contact["message"]
        );
        if ($this->db->insert('contact', $data)) {
            return TRUE;
        }
        return FALSE;
    }

}
