<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $module_array = array(
        'controller' => 'welcome',
        'model' => 'registry_model',
        'view' => 'welcome',
    );

    public function __construct() {
        parent::__construct();
        $this->load->model($this->module_array['model'], 'model');
        $this->load->helper('url');
    }

    public function index() {
        $this->load->view('welcome/welcome');
    }

    public function registry() {
//var_dump("<pre>", $this->input->post())
        if ($this->input->post()) {
            $data =  json_decode($this->input->post("data"));
                $guest = array(
                    "name" => $data->nombre,
                    "ap" => $data->ap,
                    "ap1" => $data->ap1,
                    "tel" => $data->tel,
                    "email" => $data->email,
                    "nac" => $data->nac
                );
                $reg = array(
                    "idbedroom" => $data->idbedroom,
                    "tip" => $data->tip,
                    "checkin" => $data->checkin,
                    "checkout" => $data->checkout,
                    "note" => $data->note ? $data->note : ""   
                );
                if($this->model->insGuest($guest, $reg)){
                    
                $text = "Estimado Cliente:<br/>Le agradecemos la reservación de la habitación  " . $data->tip .
                        ", registrada a nombre " . $data->nombre . " " . $data->ap . " " . $data->ap1 .
                        ", para los dias " . $data->checkin . "  -  " . $data->checkout . "<br/>
                        Pronto nos pondremos en contacto para confirmar su reservación.    
                        <div align='center'><br/><strong>Atentamente<br/>
                        Hotel escuela</strong></div>";
                $this->sendMail($data->email, $text, "Reservacion hotel escuela");
//envia registro completado
                echo json_encode(array("stat" => 1));
            } else {
//envia registro incompleto
                echo json_encode(array("stat" => 2));
            }
        }
    }

    public function contact() {
        if ($this->input->post()) {
             $data =  json_decode($this->input->post("data"));
            $guest = array(
                "name" => $data->nombre,
                "phone" => $data->phone,
                "mail" => $data->mail,
                "message" => $data->message
            );
            if ($this->model->insCon($guest)) {
//mail app
                $text = "Tienes un mensaje de contacto de: " . $data->nombre . " <br/> "
                        . $data->message . "<br/> numero de telefono: "
                        . $data->phone . "<br/> email :"
                        . $data->mail;
                if($this->sendMail("raulsanre@gmail.com", $text, "Contacto")){
//mail costumer
//                $text = "gracias contactarnos estaremos en contacto en la brevedad";
//                $this->sendMail($this->input->post('mail'), $text, "Contacto");
//envia registro completado
                echo json_encode(array("stat" => 1));
                }
            } else {
//envia registro incompleto
                echo json_encode(array("stat" => 2));
            }
        }
    }

    public function getFreeBedrooms($checkin,$checkout,$tip) {
        $des = $this->model->getFreeBedrooms($checkin,$checkout,$tip);
        if ($des) {
            echo json_encode($des);
        } 
    }

    private function sendMail($email, $message, $sub) {
//cargamos la libreria email de ci
        $this->load->library("email");

//configuracion para gmail
        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mx1.hostinger.mx',
            'smtp_port' => 2525,
            'smtp_user' => 'contacto@proyectoucq.com',
            'smtp_pass' => '221.hoES',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );
//cargamos la configuración para enviar con gmail
        $this->email->initialize($configGmail);

        $this->email->from('contacto@proyectoucq.com');
        $this->email->to($email);
        $this->email->subject($sub);
        $this->email->message($message);
        if (!$this->email->send()) {
            return FALSE;
            //con esto podemos ver el resultado
            //var_dump($this->email->print_debugger());
        }else{
            return TRUE;
        }
    }

}
