<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cos
 *
 * @author Eduardo
 */
class cos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('contact_model');
    }

        public function recibirFormulario(){
        if($this->input->post("submit")){
            //Validaciones
                                              //name del campo, titulo, restricciones
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[3]|alpha|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|min_length[3]|valid_email|trim');
            $this->form_validation->set_rules('phone', 'phone', 'required|min_length[10]');
            $this->form_validation->set_rules('message', 'message', 'required|min_length[30]');
            //Mensajes
            // %s es el nombre del campo que ha fallado
            $this->form_validation->set_message('alpha','El campo %s debe estar compuesto solo por letras');
            $this->form_validation->set_message('required','El campo %s es obligatorio'); 
            $this->form_validation->set_message('min_length[10]','El campo %s debe tener al menos 10 digitos');
            $this->form_validation->set_message('valid_email','El campo %s debe ser un email correcto');
             
             if($this->form_validation->run()!=false){ //Si la validación es correcta
                $datos["mensaje"]="Validación correcta";
             }else{
                $datos["mensaje"]="Validación incorrecta";
             }
              
             $this->load->view("formulario_view",$datos);
        }
    }
    
    
    public function sendMail() {
        $this->load->library('email');
        
        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $mail = $this->input->post('mail');
        $message = $this->input->post('message');
        
        
        //Enviar mensaje
        $insert = $this->envio_email_model->new_user($name,$phone,$mail,$message);
        $this->email->from('contacto@proyectoucq.com');
        $this->email->to("contacto@proyectoucq.com");
        $this->email->subject('Bienvenido/a a uno-de-piera.com');
        $this->email->message('<h2>Email enviado con codeigniter haciendo uso del smtp de gmail</h2><hr><br> Bienvenido al blog');
        $this->email->send();
        //Insertar en base de datos
        
        if($this->model->insertMesaage($name, $phone, $mail, $message)){
            
        }
    }

}
