<?php $this->load->view('welcome/header'); ?>  

<style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 900);
        });
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function () {
        $("#datein").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            onClose: function (selectedDate) {
                $("#dateout").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#dateout").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            onClose: function (selectedDate) {
                $("#datein").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
    function contact() {
        document.getElementById('filldetails').onsubmit = function () {
            var data = {
                nombre: $('#name').val(),
                phone: $('#phone').val(),
                mail: $('#email').val(),
                message: $('#msg').val()
            };
            $.ajax({
                type: 'POST',
                url: 'welcome/contact',
                data: {
                    data: JSON.stringify(data),
                },
                success: function (data) {
                    if (data.stat === 1) {
                        alert("gracias por sus comentarios");
                    }
                }, error: function () {
                    alert("lo sentimos porfavor contactanos al numero x-xx-xx-xx");
                },
                dataType: 'json'
            });
            return false;
        };
    }
    $(function () {
        var dialog, form, table,
                // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
                emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
                name = $("#nombre"),
                ap = $("#ap"),
                ap1 = $("#ap1"),
                tel = $("#tel"),
                email = $("#mail"),
                nac = $("#nac"),
                checkin = $("#datein"),
                checkout = $("#dateout"),
                noti = $("#not"),
                allFields = $([]).add(name).add(email).add(nac).add(ap).add(ap1).add(tel)
                .add(checkin).add(checkout).add(noti),
                tips = $(".validateTips");
        $("#selection-table").css("display", "none");
        function updateTips(t) {
            tips
                    .text(t)
                    .addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("El tamaño minimo " + n + " es de " +
                        min + " y maximo de " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        function checkRegexp(o, regexp, n) {
            if (!(regexp.test(o.val()))) {
                o.addClass("ui-state-error");
                updateTips(n);
                return false;
            } else {
                return true;
            }
        }

        function sndReg() {

            var valid = true;
            allFields.removeClass("ui-state-error");
            valid = valid && checkLength(name, "nombre", 3, 16);
            valid = valid && checkLength(ap, "apellido paterno", 3, 16);
            valid = valid && checkLength(ap1, "apellido materno", 3, 16);
            valid = valid && checkLength(tel, "telefono", 10, 13);
            valid = valid && checkLength(nac, "nacionalidad", 3, 16);
            valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
            valid = valid && checkRegexp(ap, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
            valid = valid && checkRegexp(ap1, /^[a-z]([0-9a-z_\s])+$/i, "Campo consiste en caracteres a-z , 0-9 , _ , espacios y debe comenzar con una letra.");
            valid = valid && checkRegexp(tel, /^([0-9\s])+$/i, "Campo consiste en caracteres  0-9.");
            valid = valid && checkRegexp(nac, /^[a-z]([a-z\s])+$/i, "Campo consiste en caracteres a-z espacios y debe comenzar con una letra.");
            valid = valid && checkRegexp(email, emailRegex, "eg. contacto@proyectoucq.com");
            if (valid) {
                var data = {
                    "nombre": name.val(),
                    "ap": ap.val(),
                    "ap1": ap1.val(),
                    "tel": tel.val(),
                    "email": email.val(),
                    "nac": nac.val(),
                    "tip": $("#tip option:selected").val(),
                    "checkin": checkin.val(),
                    "checkout": checkout.val(),
                    "note": noti.val(),
                    "idbedroom": $("#idbedroom").val()
                };
                $.ajax({
                    type: 'POST',
                    url: 'welcome/registry',
                    data: {
                        data: JSON.stringify(data),
                    },
                    success: function (data) {
                        if (data.stat === 1) {
                            alert("solicitud enviada");
                            location.reload();
                        } else {
                            alert("lo sentimos a ocurrido un error porfavor contactanos al numero x-xx-xx-xx");
                        }
                    }, error: function (xhr, ajaxOptions, thrownError) {
                        alert("es pocible que tu correo este incorrecto verifica por favor e intentalo de nuevo\n\
                                        en caso contrario porfavor contactanos al numero x-xx-xx-xx");
                    },
                    dataType: 'json'
                });
                dialog.dialog("close");
            }
            return valid;
        }

        dialog = $("#dialog-form").dialog({
            autoOpen: false,
            height: 600,
            width: 450,
            modal: true,
            buttons: {
                "Envia registro": sndReg,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[ 0 ].reset();
                allFields.removeClass("ui-state-error");
            }
        });
        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            sndReg();
        });
        $("#create-user").button().on("click", function () {
            $("#selection-table").css("display", "block");
            var url = "welcome/getFreeBedrooms/" + checkin.val() + "/" + checkout.val() + "/" + $("#tip option:selected").val() + "";
            if (table) {
                table.ajax.url(url).load();
            } else {
                table = $('#example').DataTable({
                    "ajax": url,
                    "searching": false,
                    "lengthChange": false,
                    "columns": [
                        {"data": "number"},
                        {"data": "description"},
                        {"data": "price"}
                        
                    ]
                });
            }

            $('#example tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    $('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    $.each(table.row(this).data(), function (index, value) {
                        if(index=="idbedrooms"){
                         $("#idbedroom").val(value);
                        }
                    });
                    dialog.dialog("open");
                }
            });
        });
    });</script>
<!-- swipe box js -->

<script src="resources/js/jquery.swipebox.min.js"></script> 
<script type="text/javascript">
    jQuery(function ($) {
        $(".swipebox").swipebox();
    });</script>
<!-- //swipe box js -->
<!--animate-->
<link href="resources/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="resources/js/wow.min.js"></script>
<script>
    new WOW().init();</script>

</head>
<body>
    <!--start-home-->
    <div class="banner" id="home">
        <div class="header-bottom wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">
            <div class="container">
                <div class="fixed-header">
                    <!--logo-->
                    <div class="logo">
                        <a href="#home"><img src="resources/images/logo_top.png" alt=""></a>
                        
                    </div>
                    <!--//logo-->
                    <div class="top-menu">
                        <span class="menu"> </span>
                        <nav class="link-effect-4" id="link-effect-4">
                            <ul>
                                <li class="active"><a data-hover="Home" href="#home">Home</a></li>
                                <li><a data-hover="Nosotros" href="#about" class="scroll">Nosotros</a></li>
                                <li><a data-hover="Servicios" href="#services" class="scroll">Servicios</a></li>
                                <li><a data-hover="Habitaciones" href="#team" class="scroll">Habitaciones</a></li>
                                <li><a data-hover="Reservación" href="#reservation" class="scroll">Reservación</a></li>
                                <li><a data-hover="Galería" href="#gallery" class="scroll">Galería</a></li>
                                <li><a data-hover="Contacto" href="#contact" class="scroll">Contacto</a></li>

                            </ul>
                        </nav>
                    </div>
                    <!-- script-for-menu -->
                    <script>
                        $("span.menu").click(function () {
                            $(".top-menu ul").slideToggle("slow", function () {
                            });
                        });
                    </script>
                    <!-- script-for-menu -->

                    <div class="clearfix"></div>
                    <script>
                        $(document).ready(function () {
                            var navoffeset = $(".header-bottom").offset().top;
                            $(window).scroll(function () {
                                var scrollpos = $(window).scrollTop();
                                if (scrollpos >= navoffeset) {
                                    $(".header-bottom").addClass("fixed");
                                } else {
                                    $(".header-bottom").removeClass("fixed");
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="banner-slider" id="home">
            <div class="callbacks_container">
                <ul class="rslides" id="slider4">
                    <li>
                        <div class="banner-info">
                            <h3 class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">Bienvenido</h3>
                            <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">
                                <a href="#home"><img class="img-responsive" src="resources/images/logo.png" alt=""></a>
                            </p>
                            <div class="arrows wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s"><img src="resources/images/border.png" alt="border"/></div>
                            <span class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">LISTOS PARA TÍ</span>
                        </div>
                    </li>
                    <!--para evitar cagarla-->
                    <!--                    <li>
                                            <div class="banner-info">
                                                <h3 class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">HOTLE </h3>
                                                <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">EJECUTIVAS</p>
                                                <div class="arrows wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s"><img src="resources/images/border.png" alt="border"/></div>
                                                <span class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">READY TO BE OPENED</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="banner-info">
                                                <h3 class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">Delicious</h3>
                                                <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">FRESH FOODS</p>
                                                <div class="arrows wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s"><img src="resources/images/border.png" alt="border"/></div>
                                                <span class="wow slideInUpslideInLeft"  data-wow-duration="1s" data-wow-delay=".3s">READY TO BE OPENED</span>
                                            </div>
                                        </li>-->
                </ul>
            </div>
            <!--banner Slider starts Here-->
            <script src="resources/js/responsiveslides.min.js"></script>
            <script>
                        // You can also use "$(window).load(function() {"
                        $(function () {
                            // Slideshow 4
                            $("#slider4").responsiveSlides({
                                auto: true,
                                pager: true,
                                nav: false,
                                speed: 500,
                                namespace: "callbacks",
                                before: function () {
                                    $('.events').append("<li>before event fired.</li>");
                                },
                                after: function () {
                                    $('.events').append("<li>after event fired.</li>");
                                }
                            });
                        });
            </script>
            <!--banner Slider starts Here-->
        </div>

        <div class="down"><a class="scroll" href="#services"><img src="resources/images/down.png" alt=""></a></div>
    </div>

    <!--/products-->
    <div class="about" id="about">
        <div class="container">
            <!--/about-section-->
            <div class="about-section">
                <div class="col-md-7 ab-left">
                    <div class="grid">
                        <div class="h-f wow slideInLeft"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Equipo_Cuau.jpg" alt="img25"/>
                                <figcaption>
                                    <h4>Equipo <span>UCQ</span></h4>
                                    <p>Somos quienes hacemos posible éste proyecto educativo - productivo.</p>    
                                
                                </figcaption>			
                            </figure>

                        </div>
                        <div class="h-f wow slideInLeft"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/1_opt.jpg" height="210" width="330" alt="img25"/>
                                <figcaption>
                                     <h4>Hotel - Escuela <span>Amealco</span></h4>
                                    <p>Conocenos y se parte de ésta experiencia.</p>   
                                </figcaption>			
                            </figure>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-5 ab-text">
                    <h3 class="tittle wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">Bienvenido!!!</h3>
                    <div class="arrows-two wow slideInDown"  data-wow-duration="1s" data-wow-delay=".5s"><img src="resources/images/border.png" alt="border"/></div>
                    <p class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">
                    Hotel - Escuela Amealco es un Hotel con clasificación tres estrellas, ubicado en el corazón del hermoso municipio de Amealco 
                    de Bonfil en el estado de Querétaro.
                    
                    </p>
                    <div class="start wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">
                        <!-- este de momento no 
                        <a href="single.html" class="hvr-bounce-to-bottom">Empecemos</a>
                        -->
                    </div>

                </div>
                <div class="clearfix"> </div>
            </div>
            <!--//about-section-->
            <!--/about-section2-->
            <div class="about-section">
                <div class="col-md-5 ab-text two">
                    <h3 class="tittle wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">Acerca de</h3>
                    <div class="arrows-two wow slideInDown"  data-wow-duration="1s" data-wow-delay=".5s">
                        <img src="resources/images/border.png" alt="border"/>
                    </div>
                    <p class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">
                        Es operado mediante una alianza con la prestigiosa cadena de Hoteles Misión y la Universidad Cuauhtémoc Querétaro y cuyo objetivo 
                        es cubrir las necesidades en el sector turistico de la zona apoyando a la comunidad educativa de diferentes licenciaturas impartidas
                        en la universidad.
                    
                    </p>
                    <div class="start wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">
                        <!-- este de momento no
                        <a href="single.html" class="hvr-bounce-to-bottom">Conozca Mas</a>
                        -->
                    </div>

                </div>
                <div class="col-md-7 ab-left">
                    <div class="grid">
                        <div class="h-f  wow slideInRight"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Ad-1.jpg" alt="img25"/> 
                                <figcaption>
                                    <h4>Artesanías <span>Regionales</span></h4>
                                    <p>Jarro de Barro</p> 
                                </figcaption>			
                            </figure>

                        </div>
                        <div class="h-f  wow slideInRight"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Ad-2.jpg" alt="img25"/>
                                <figcaption>
                                    
                                    <h4>Artesanías <span>Regionales</span></h4>
                                    <p>Muñeca tradicional</p> 

                                </figcaption>			
                            </figure>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!--//about-section2-->
        </div>
    </div>
    <!--//products-->
    <!-- service-type-grid -->
    <div class="service" id="services">
        <div class="container">
            <h3 class="tittle">Nuestros Servicios</h3>
            <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
            <div class="inst-grids">
                <div class="col-md-3 services-gd text-center wow slideInLeft"  data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve1.png" alt=" " /></a>
                    </div>
                    <h4>Restaurante</h4>
                    <p>Los mejores platillos tipicos de la region te deleitaran el paladar.</p>
                </div>
                <div class="col-md-3 services-gd text-center wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve2.png" alt=" " /></a>
                    </div>
                    <h4>Sala de Reuniones</h4>
                    <p>Realiza tu evento con nosotros y conoce la región.</p>
                </div>
                <div class="col-md-3 services-gd text-center wow slideInUp"  data-wow-duration="1s" data-wow-delay=".2s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve3.png" alt=" " /></a>
                    </div>
                    <h4>Servicio al Cuarto</h4>
                    <p>Atención al cliente personalizada dedicada a mejorar tu estadia y concentir tus ordenes.</p>
                </div>
                <div class="col-md-3 services-gd text-center wow slideInRight"  data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/Wifi.png" alt=" " /></a>
                    </div>
                    <h4>Wi-fi</h4>
                    <p>Tenemos Wi-fi disponible en todas las habitaciones y areas comunes, asi como estacionamiento gratuito.</p>
                </div>
                <div class="clearfix"> </div>		
            </div>

        </div>
    </div>
    <!-- //service-type-grid -->
    <!--start-services-->
    <div class="team-section" id="team">
        <div class="container">
            <h3 class="tittle">Nuestras Habitaciones</h3>
            <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
            <div class="box2">
                <div class="col-md-4 s-1 wow slideInLeft" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/Hab-3.jpg" width="250" height="330" alt="border">
                            <div class="mask">
                                <h4>Estándar</h4>
                                <p>Cuenta con 2 camas tipo matrimoniales, una pantalla de 32´, internet, 2 toallas de
                                baño, secadora de cabello, vaso, botellín de agua, jabón, shampoo y crema
                                corporal.</p>     
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="col-md-4 s-2 wow slideInLeft" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/Hab-1.jpg" width="300" height="330" alt="">
                            <div class="mask">
                                <h4>Junior</h4>
                                <p>Cuenta con cama queensize, televisión, internet, tocador, 2 toallas de baño, toalla de
                                    mano, media sala vaso, botellín de agua, jabón, shampoo y crema corporal.</p>
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="col-md-2 s-3 wow slideInRight" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/Hab-2.jpg" width="300" height="330" alt="">
                            <div class="mask">
                                <h4>Master</h4>
                                <p>Cuenta con cama tipo quinsay, sala completa, chimenea, 1 televisión, internet, 2
                                toallas de baño, toalla facial, tapete de baño, vaso, botellín de agua, jabón,
                                shampoo y crema corporal.</p>
                               
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-team-->

    <!--start-banner-bottom-->
    <!--/reviews-->
    <div id="review" class="reviews">
        <div class="col-md-6 test-left-img">
        </div>
        <div class="col-md-6 test-monials">
            <h3 class="tittle">Testimonios</h3>
            <div class="arrows-serve test"><img src="resources/images/border.png" alt="border"></div>
            <!--//screen-gallery-->
            <div class="sreen-gallery-cursual">
                <!-- required-js-files-->
                <link href="resources/css/owl.carousel.css" rel="stylesheet">
                <script src="resources/js/owl.carousel.js"></script>
                <script>
                        $(document).ready(function () {
                            $("#owl-demo").owlCarousel({
                                items: 1,
                                lazyLoad: true,
                                autoPlay: true,
                                navigation: false,
                                navigationText: false,
                                pagination: true,
                            });
                        });
                </script>
                <!--//required-js-files-->
                <div id="owl-demo" class="owl-carousel">
                    <div class="item-owl">
                        <div class="test-review">
                            <p class="wow fadeInUp"  data-wow-duration=".8s" data-wow-delay=".4s"><img src="resources/images/left-quotes.png" alt=""> Excelente servicio y atención. <img src="resources/images/right-quotes.png" alt=""></p>
                            <img src="resources/images/t3.jpg" class="img-responsive" alt=""/>
                            <h5 class="wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Martin H. Joseph</h5>
                        </div>
                    </div>
                    <div class="item-owl">
                        <div class="test-review">
                            <p class="wow fadeInUp"  data-wow-duration=".8s" data-wow-delay=".4s"> <img src="resources/images/left-quotes.png" alt="">Amealco, un lugar maravilloso! Y el hotel magnifico.<img src="resources/images/right-quotes.png" alt=""></p>
                            <img src="resources/images/t2.jpg" class="img-responsive" alt=""/>
                            <h5 class="wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Dennis Pal,</h5>
                        </div>
                    </div>
                    <div class="item-owl">
                        <div class="test-review">
                            <p class="wow fadeInUp"  data-wow-duration=".8s" data-wow-delay=".4s"><img src="resources/images/left-quotes.png" alt=""> Por supuesto que volveré !!<img src="resources/images/right-quotes.png" alt=""></p>
                            <img src="resources/images/t1.jpg" class="img-responsive" alt=""/>
                            <h5 class="wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Martin H.Wilson</h5>
                        </div>
                    </div>
                </div>
                <!--//screen-gallery-->
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!--//reviews-->
    <!--reservation-->
    <div class="reservation" id="reservation">
        <div class="container">
            <div class="reservation-info">
                <h3 class="tittle reserve">Reserva</h3>
                <div class="arrows-reserve"><img src="resources/images/border.png" alt="border"></div>
                <div class="book-reservation wow slideInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <div class="row">
                        <div class="col-md-4 form-left">
                            <label><i class="glyphicon glyphicon-calendar"></i> Inicio :</label>
                            <input id="datein" name="datein" type="text">
                        </div>
                        <div class="col-md-4 form-left">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fin :</label>
                            <input id="dateout" name="dateout" type="text">
                        </div>

                        <div class="col-md-4 form-left">
                            <label><i class="glyphicon glyphicon-user"></i>Tipo de Habitacion :</label>
                            <select id="tip" name="tip" class="form-control">
                                <option value="ESTANDAR">Estándar</option>
                                <option value="JUNIOR">Junior</option>
                                <option value="MASTER">Máster</option>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix"> </div>
                    <div class="make wow shake" data-wow-duration="1s" data-wow-delay=".5s">
                        <input type="submit"  id="create-user"   value="Reserva">
                    </div>
                </div>
                <div id="selection-table" style="background-color: white" >
                    <table id="example" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Numero</th>
                                <th>Descripcion</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Numero</th>
                                <th>Descripcion</th>
                                <th>Precio</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>	
<!--//reservation-->

<!--Gallery-->
<div class="gallery" id="gallery">
    <div class="container">
        <h3 class="tittle">Galeria</h3>
        <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
        <div class="gallery-grids">
            <div class="col-md-6 baner-top wow fadeInRight animated" data-wow-delay=".5s">
                <a href="resources/images/folder/1_opt(1).jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/bottom.jpg" width="350" height="350" alt=""/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Restaurante</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 baner-top wow fadeInLeft animated" data-wow-delay=".5s">
                <a href="resources/images/folder/1_opt.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/1_opt.jpg" width="350" height="350" alt=""/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Ven a visitarnos</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInUp animated" data-wow-delay=".5s">
                <a href="resources/images/6.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/6.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Salón de Eventos</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInDown animated" data-wow-delay=".5s">
                <a href="resources/images/folder/8.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/8.jpg" alt=""/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Fachada Restaurante</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInUp animated" data-wow-delay=".5s">
                <a href="resources/images/folder/Hab-2.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/Hab-2.jpg" width="200" height="180" alt=""/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Habitaciones Hotel escuela</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInDown animated" data-wow-delay=".5s">
                <a href="resources/images/folder/Hab-3.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/Hab-3.jpg" width="200" height="180" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Habitaciones Hotel escuela</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 baner-top wow fadeInRight animated" data-wow-delay=".5s">
                <a href="resources/images/folder/2.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/Iglesia.jpg" alt=" "/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Parroquia Santa María</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 baner-top wow fadeInLeft animated" data-wow-delay=".5s">
                <a href="resources/images/folder/Arbol.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/Arbol.jpg" alt=""/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Alrededores Amealco</h4>
                                <span class="separator"></span>
                                <!--<p></p>-->
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //gallery -->

<!--bottom-->
<!--<div class="bottom">
    <div class="container">
        <div class="bottom-top">
            <h3 class=" wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">Texto</h3>
            <span>Texto</span>
            <p class="wow slideInDown" data-wow-duration="1s" data-wow-delay=".5s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since, Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            <div class="start wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">
                <a href="single.html" class="hvr-bounce-to-bottom">Conozca Mas</a>
            </div>
        </div>
    </div>
</div>-->

<!--/contact-->
<div class="section-contact" id="contact">
    <div class="container">
        <div class="contact-main">
            <div class="col-md-6 contact-grid wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".3s">
                <h3 class="tittle wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Contactanos</h3>
                <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
               <!-- <p class="wel-text wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>-->
                <form id="filldetails" >
                    <div class="field name-box">
                        <input type="text" id="name" placeholder="Introduzca su nombre completo" required=""/>
                        <label for="name">Nombre</label>
                        <span class="ss-icon">check</span>
                    </div>

                    <div class="field email-box">
                        <input type="text" id="email" placeholder="example@email.com" required=""/>
                        <label for="email">Email</label>
                        <span class="ss-icon">check</span>
                    </div>

                    <div class="field phonenum-box">
                        <input type="text" id="phone" placeholder="Ingrese su numero telefonico" required=""/>
                        <label for="phone">Telefono</label>
                        <span class="ss-icon">check</span>
                    </div>

                    <div class="field msg-box">
                        <textarea id="msg" rows="4" placeholder="Ingrese su mensaje..." required=""/></textarea>
                        <label for="msg">Mensaje</label>
                        <span class="ss-icon">check</span>
                    </div>
                    <div class="send wow shake"  data-wow-duration="1s" data-wow-delay=".3s">
                        <input type="submit" onclick="contact()" value="Enviar" >
                    </div>

                </form>

            </div>
            <div class="col-md-6 contact-in wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".5s">
                <h4 class="info">Información de Contacto </h4>
                <p class="para1">
                    Si deseas mayor información, contáctanos mediante el siguiente formulario, por correo electrónico o por teléfono,
                    siempre estaremos para servirte.
                </p>
                <div class="con-top">
                    <h4>Queretaro, México</h4>
                    <ul>
                        <li>Dirección IV Centenario No. 45</li> 
                        <li>Colonia Centro,</li>  
                        <li>Amealco de Bonfil</li> 
                        <li>CP. 76850 </li>
                        <!--<li>Call Centre Time : 7am to 11pm</li>-->
                        <li><a href="mailto:contacto@proyectoucq.com">contacto@proyectoucq.com</a></li>
                    </ul>
                </div>
                <!--<div class="con-top two">
                    <h4>Hawaii</h4>
                    <ul>
                        <li>4th Floor dolor sit amet,</li> 
                        <li># Grand Hyatt New York Road,</li>   
                        <li>Opp. adipiscing elit,</li> 
                        <li>Hawaii - 96815</li>
                        <li>Ph:3698521475 </li>
                        <li>Call Centre Time : 7am to 11pm</li>	
                        <li><a href="mailto:info@example.com">mail@example.com</a></li>
                    </ul>								
                </div>-->
            </div>

            <div class="clearfix"> </div>
        </div>
        <!--map-->
        <div class="map wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".5s">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3744.735386836671!2d-100.14574068539123!3d20.1867402213644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d31a49424b79f5%3A0xafc5a19ab8eb1343!2sHotel+Amealco!5e0!3m2!1ses-419!2smx!4v1470204638413" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
        <!--//map-->
    </div>
</div>
<!--//contact-->
<!-- dialog -->
<div id="dialog-form" title="Registro">
    <p>Para terminar la reservacion llenar los datos</p>
    <p class="validateTips">Todos los campos son requeridos</p>
    <form>
        <fieldset>
            <input type="hidden" id="idbedroom">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" value="" class="text ui-widget-content ui-corner-all">
            <label for="ap">Apellido paterno</label>
            <input type="text" name="ap" id="ap" value="" class="text ui-widget-content ui-corner-all">
            <label for="ap1">Apellido materno</label>
            <input type="text" name="ap1" id="ap1" value="" class="text ui-widget-content ui-corner-all">
            <label for="tel">Telefono</label>
            <input type="tel" name="tel" id="tel" value="" class="text ui-widget-content ui-corner-all">
            <label for="mail">Email</label>
            <input type="mail" name="mail" id="mail" value="" class="text ui-widget-content ui-corner-all">
            <label for="nac">Nacionalidad</label>
            <input type="text" name="nac" id="nac" value="" class="text ui-widget-content ui-corner-all">
            <label for="not">Notas</label>
            <input type="text" name="not" id="not" value="" class="text ui-widget-content ui-corner-all">
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
    </form>
</div>
<!-- end dialog -->
<?php $this->load->view('welcome/footer'); ?>
