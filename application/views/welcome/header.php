<!DOCTYPE HTML>
<html>
    <head>
        <title>Hotel Escuela</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Honest Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
        <script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="resources/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="resources/css/iconeffects.css" rel='stylesheet' type='text/css' />
        <link href="resources/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" href="resources/css/swipebox.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="resources/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="resources/js/move-top.js"></script>
        <script type="text/javascript" src="resources/js/easing.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>   
        <!--/web-font-->
        <link href='//fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Merriweather+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <!--/script-->
       <!--data table-->
        <link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css' />
        <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
        
         <link rel="shortcut icon" href="favicon.ico" />
        <link rel="apple-touch-icon" href="favicon.png" />