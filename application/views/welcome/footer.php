<div class="footer text-center">
    <div class="container">
        <!--logo2-->
        <div class="banner-info">
            
            <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">
                <a href="#home"><img class="img-responsive" src="resources/images/logo.png" alt=""></a>
            </p>
            
        </div>
        <!--//logo2-->
        <a href="#" class="flag_tag2">Búscanos en...</a>
        <ul class="social wow slideInDown" data-wow-duration="1s" data-wow-delay=".3s">
            
            <li><a href="https://www.facebook.com/ucqro/?ref=ts&fref=ts" class="fb"> </a></li>
            <li><a href="https://twitter.com/ucuauhtemoc" class="tw"></a></li>
            
            <div class="clearfix"></div>
        </ul>
        <p class="copy-right wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".3s">&copy; Hotel escuela | Design by <a href="http://w3layouts.com">W3layouts</a></p>

    </div>
</div>
<!--start-smooth-scrolling-->
<script type="text/javascript">
    $(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear' 
         };
         */

        $().UItoTop({easingType: 'easeOutQuart'});

    });
</script>
<!--end-smooth-scrolling-->
<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>