<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
    <head>
        <title>Hotel Escuela</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Honest Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
        <script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="resources/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="resources/css/iconeffects.css" rel='stylesheet' type='text/css' />
        <link href="resources/css/style.css" rel='stylesheet' type='text/css' />	
        <link rel="stylesheet" href="resources/css/swipebox.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="resources/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="resources/js/move-top.js"></script>
        <script type="text/javascript" src="resources/js/easing.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>   
        <!--/web-font-->
        <link href='//fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Merriweather+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <!--/script-->
    </head>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 900);
            });
        });
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $(function () {
            $("#datein").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onClose: function (selectedDate) {
                    $("#dateout").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#dateout").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onClose: function (selectedDate) {
                    $("#datein").datepicker("option", "maxDate", selectedDate);
                }
            });
        });
    </script>
    <!-- swipe box js -->

    <script src="resources/js/jquery.swipebox.min.js"></script> 
    <script type="text/javascript">
        jQuery(function ($) {
            $(".swipebox").swipebox();
        });
    </script>
    <!-- //swipe box js -->
    <!--animate-->
    <link href="resources/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="resources/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

</head>
<body>
    <!--start-home-->
    <div class="banner" id="home">
        <div class="header-bottom wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">
            <div class="container">
                <div class="fixed-header">
                    <!--logo-->
                    <div class="logo">
                        <a href="index.html"><img src="resources/images/logo_top.png" alt=""></a>
                        <p>Estudiando para tu confort</p>
                    </div>
                    <!--//logo-->
                    <div class="top-menu">
                        <span class="menu"> </span>
                        <nav class="link-effect-4" id="link-effect-4">
                            <ul>
                                <li class="active"><a data-hover="Home" href="hotel-escuela/">Home</a></li>
                                <li><a data-hover="Nosotros" href="#about" class="scroll">Nosotros</a></li>
                                <li><a data-hover="Servicios" href="#services" class="scroll">Servicios</a></li>
                                <li><a data-hover="Habitaciones" href="#team" class="scroll">Habitaciones</a></li>
                                <li><a data-hover="Reservation" href="#reservation" class="scroll">Reservacion</a></li>
                                <li><a data-hover="Galeria" href="#gallery" class="scroll">Galeria</a></li>
                                <li><a data-hover="Contacto" href="#contact" class="scroll">Contacto</a></li>

                            </ul>
                        </nav>
                    </div>
                    <!-- script-for-menu -->
                    <script>
                        $("span.menu").click(function () {
                            $(".top-menu ul").slideToggle("slow", function () {
                            });
                        });
                    </script>
                    <!-- script-for-menu -->

                    <div class="clearfix"></div>
                    <script>
                        $(document).ready(function () {
                            var navoffeset = $(".header-bottom").offset().top;
                            $(window).scroll(function () {
                                var scrollpos = $(window).scrollTop();
                                if (scrollpos >= navoffeset) {
                                    $(".header-bottom").addClass("fixed");
                                } else {
                                    $(".header-bottom").removeClass("fixed");
                                }
                            });

                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="banner-slider">
            <div class="callbacks_container">
                <ul class="rslides" id="slider4">
                    <li>
                        <div class="banner-info">
                            <h3 class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">Bienvenido</h3>
                            <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">AL CONFORT EJECUTIVO</p>
                            <div class="arrows wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s"><img src="resources/images/border.png" alt="border"/></div>
                            <span class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">LISTO PARA TI</span>
                        </div>
                    </li>
                    <!--para evitar cagarla-->
                    <!--                    <li>
                                            <div class="banner-info">
                                                <h3 class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">HOTLE </h3>
                                                <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">EJECUTIVAS</p>
                                                <div class="arrows wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s"><img src="resources/images/border.png" alt="border"/></div>
                                                <span class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">READY TO BE OPENED</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="banner-info">
                                                <h3 class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">Delicious</h3>
                                                <p class="wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">FRESH FOODS</p>
                                                <div class="arrows wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s"><img src="resources/images/border.png" alt="border"/></div>
                                                <span class="wow slideInUpslideInLeft"  data-wow-duration="1s" data-wow-delay=".3s">READY TO BE OPENED</span>
                                            </div>
                                        </li>-->
                </ul>
            </div>
            <!--banner Slider starts Here-->
            <script src="resources/js/responsiveslides.min.js"></script>
            <script>
                        // You can also use "$(window).load(function() {"
                        $(function () {
                            // Slideshow 4
                            $("#slider4").responsiveSlides({
                                auto: true,
                                pager: true,
                                nav: false,
                                speed: 500,
                                namespace: "callbacks",
                                before: function () {
                                    $('.events').append("<li>before event fired.</li>");
                                },
                                after: function () {
                                    $('.events').append("<li>after event fired.</li>");
                                }
                            });

                        });
            </script>
            <!--banner Slider starts Here-->
        </div>

        <div class="down"><a class="scroll" href="#services"><img src="resources/images/down.png" alt=""></a></div>
    </div>

    <!--/products-->
    <div class="about" id="about">
        <div class="container">
            <!--/about-section-->
            <div class="about-section">
                <div class="col-md-7 ab-left">
                    <div class="grid">
                        <div class="h-f wow slideInLeft"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Hab-1.jpg" alt="img25"/>
                                <figcaption>
                                    <h4>Hotel <span></span></h4>
                                    <p></p>

                                </figcaption>			
                            </figure>

                        </div>
                        <div class="h-f wow slideInLeft"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Hab-2.jpg" alt="img25"/>
                                <figcaption>
                                    <h4>Hotel <span></span></h4>
                                    <p>.</p>

                                </figcaption>			
                            </figure>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-5 ab-text">
                    <h3 class="tittle wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">Bienvenido!!!</h3>
                    <div class="arrows-two wow slideInDown"  data-wow-duration="1s" data-wow-delay=".5s"><img src="resources/images/border.png" alt="border"/></div>
                    <p class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">Lorem ipsum Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                    <div class="start wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">
                        <a href="single.html" class="hvr-bounce-to-bottom">Empecemos</a>
                    </div>

                </div>
                <div class="clearfix"> </div>
            </div>
            <!--//about-section-->
            <!--/about-section2-->
            <div class="about-section">
                <div class="col-md-5 ab-text two">
                    <h3 class="tittle wow slideInDown"  data-wow-duration="1s" data-wow-delay=".3s">Acerca de</h3>
                    <div class="arrows-two wow slideInDown"  data-wow-duration="1s" data-wow-delay=".5s"><img src="resources/images/border.png" alt="border"/></div>
                    <p class="wow slideInUp"  data-wow-duration="1s" data-wow-delay=".3s">Lorem ipsum Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                    <div class="start wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">
                        <a href="single.html" class="hvr-bounce-to-bottom">Conozca Mas</a>
                    </div>

                </div>
                <div class="col-md-7 ab-left">
                    <div class="grid">
                        <div class="h-f  wow slideInRight"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Hab-3.jpg" alt="img25"/>
                                <figcaption>
                                    <h4>Hotel <span>5 estrellas</span></h4>
                                    <p></p>

                                </figcaption>			
                            </figure>

                        </div>
                        <div class="h-f  wow slideInRight"  data-wow-duration="1s" data-wow-delay=".2s">
                            <figure class="effect-jazz">
                                <img src="resources/images/folder/Hab11-4.jpg" alt="img25"/>
                                <figcaption>
                                    <h4>Hotel <span>5 estrellas</span></h4>
                                    <p></p>

                                </figcaption>			
                            </figure>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!--//about-section2-->
        </div>
    </div>
    <!--//products-->
    <!-- service-type-grid -->
    <div class="service" id="services">
        <div class="container">
            <h3 class="tittle">Nuestros Servicios</h3>
            <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
            <div class="inst-grids">
                <div class="col-md-3 services-gd text-center wow slideInLeft"  data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve1.png" alt=" " /></a>
                    </div>
                    <h4>Nuestro Menu</h4>
                    <p>Los mejores platillos.</p>
                </div>
                <div class="col-md-3 services-gd text-center wow slideInDown"  data-wow-duration="1s" data-wow-delay=".2s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve2.png" alt=" " /></a>
                    </div>
                    <h4>Reservacion</h4>
                    <p>Reserva aqui mismo.</p>
                </div>
                <div class="col-md-3 services-gd text-center wow slideInUp"  data-wow-duration="1s" data-wow-delay=".2s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve3.png" alt=" " /></a>
                    </div>
                    <h4>Servicio al Cuarto</h4>
                    <p>Atención al cliente personalizada.</p>
                </div>
                <div class="col-md-3 services-gd text-center wow slideInRight"  data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                        <a href="#" class="hi-icon"><img src="resources/images/serve4.png" alt=" " /></a>
                    </div>
                    <h4>Chef</h4>
                    <p>La gran experiencia de los chef´s deleitera el su paladar.</p>
                </div>
                <div class="clearfix"> </div>		
            </div>

        </div>
    </div>
    <!-- //service-type-grid -->
    <!--start-services-->
    <div class="team-section" id="team">
        <div class="container">
            <h3 class="tittle">Nuestro comedor tradicional</h3>
            <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
            <div class="box2">
                <div class="col-md-3 s-1 wow slideInLeft" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/3_opt.jpg" alt="chef">
                            <div class="mask">
                                <h4>Vivamus moles tie gravida turpis</h4>
                                <p>A wonderful serenity has taken possession of my entire soul,  I enjoy with my whole heart.</p>
                                <p class="p2">A wonderful serenity has taken possession of my entire soul, I enjoy with my whole heart.</p>
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="col-md-3 s-2 wow slideInLeft" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/4_opt.jpg" alt="chef">
                            <div class="mask">
                                <h4>Vivamus moles tie gravida turpis</h4>
                                <p>A wonderful serenity has taken possession of my entire soul,  I enjoy with my whole heart.</p>
                                <p class="p2">A wonderful serenity has taken possession of my entire soul, I enjoy with my whole heart.</p>
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="col-md-3 s-3 wow slideInRight" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/7_opt.jpg" alt="chef">
                            <div class="mask">
                                <h4>Vivamus moles tie gravida turpis</h4>
                                <p>A wonderful serenity has taken possession of my entire soul,  I enjoy with my whole heart.</p>
                                <p class="p2">A wonderful serenity has taken possession of my entire soul, I enjoy with my whole heart.</p>
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="col-md-3 s-4 wow slideInRight" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#">
                        <div class="view view-fifth">
                            <img src="resources/images/folder/5_opt.jpg" alt="chef">
                            <div class="mask">
                                <h4>Vivamus moles tie gravida turpis</h4>
                                <p>A wonderful serenity has taken possession of my entire soul,  I enjoy with my whole heart.</p>
                                <p class="p2">A wonderful serenity has taken possession of my entire soul, I enjoy with my whole heart.</p>
                            </div>

                        </div>
                    </a>
                    <h3></h3>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-team-->

    <!--start-banner-bottom-->
    <!--/reviews-->
    <div id="review" class="reviews">
        <div class="col-md-6 test-left-img">
        </div>
        <div class="col-md-6 test-monials">
            <h3 class="tittle">Testimonios</h3>
            <div class="arrows-serve test"><img src="resources/images/border.png" alt="border"></div>
            <!--//screen-gallery-->
            <div class="sreen-gallery-cursual">
                <!-- required-js-files-->
                <link href="resources/css/owl.carousel.css" rel="stylesheet">
                <script src="resources/js/owl.carousel.js"></script>
                <script>
                        $(document).ready(function () {
                            $("#owl-demo").owlCarousel({
                                items: 1,
                                lazyLoad: true,
                                autoPlay: true,
                                navigation: false,
                                navigationText: false,
                                pagination: true,
                            });
                        });
                </script>
                <!--//required-js-files-->
                <div id="owl-demo" class="owl-carousel">
                    <div class="item-owl">
                        <div class="test-review">
                            <p class="wow fadeInUp"  data-wow-duration=".8s" data-wow-delay=".4s"><img src="resources/images/left-quotes.png" alt=""> Comentario 1 <img src="images/right-quotes.png" alt=""></p>
                            <img src="resources/images/t3.jpg" class="img-responsive" alt=""/>
                            <h5 class="wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Martin H. Joseph</h5>
                        </div>
                    </div>
                    <div class="item-owl">
                        <div class="test-review">
                            <p class="wow fadeInUp"  data-wow-duration=".8s" data-wow-delay=".4s"> <img src="resources/images/left-quotes.png" alt="">Comentario 2<img src="images/right-quotes.png" alt=""></p>
                            <img src="resources/images/t2.jpg" class="img-responsive" alt=""/>
                            <h5 class="wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Dennis Pal,</h5>
                        </div>
                    </div>
                    <div class="item-owl">
                        <div class="test-review">
                            <p class="wow fadeInUp"  data-wow-duration=".8s" data-wow-delay=".4s"><img src="resources/images/left-quotes.png" alt=""> Comentario 3<img src="images/right-quotes.png" alt=""></p>
                            <img src="resources/images/t1.jpg" class="img-responsive" alt=""/>
                            <h5 class="wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Martin H.Wilson</h5>
                        </div>
                    </div>
                </div>
                <!--//screen-gallery-->
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!--//reviews-->
    <!--reservation-->
    <div class="reservation" id="reservation">
        <div class="container">
            <div class="reservation-info">
                <h3 class="tittle reserve">Reserva</h3>
                <div class="arrows-reserve"><img src="resources/images/border.png" alt="border"></div>
                <div class="book-reservation wow slideInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <form action="welcome/registry" method="post">
                        <div class="row">
                        <div class="col-md-4 form-left">
                            <label><i class="glyphicon glyphicon-calendar"></i> Inicio :</label>
                            <input id="datein" name="datein" type="text">
                        </div>
                        <div class="col-md-4 form-left">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fin :</label>
                            <input id="dateout" name="dateout" type="text">
                        </div>
                        </div>
                        
                        <div class="row" style="align-content: center">
                            <div class="col-md-4 form-rigth">
                                <label><i class="glyphicon glyphicon-user"></i> Numero de personas :</label>
                                <select id="nps" name="nps" class="form-control">
                                    <option value="1">1 Persona</option>
                                    <option value="2">2 Personas</option>
                                    <option value="3">3 Personas</option>
                                    <option value="4">4 Personas</option>
                                    <option value="5">5 Personas</option>
                                </select>
                            </div>
                        </div>    
                        <div class="clearfix"> </div>
                        <div class="make wow shake" data-wow-duration="1s" data-wow-delay=".5s">
                            <input type="submit" value="Reserva">
                        </div>
                    </form>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>	
<!--//reservation-->

<!--Gallery-->
<div class="gallery" id="gallery">
    <div class="container">
        <h3 class="tittle">Galeria</h3>
        <div class="arrows-serve"><img src="resources/images/border.png" alt="border"></div>
        <div class="gallery-grids">
            <div class="col-md-6 baner-top wow fadeInRight animated" data-wow-delay=".5s">
                <a href="resources/images/folder/1_opt(1).jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/1_opt(1).jpg" alt=" "/>
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 baner-top wow fadeInLeft animated" data-wow-delay=".5s">
                <a href="resources/images/folder/1_opt.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/1_opt.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Ven a visitarnos</h4>
                                <span class="separator"></span>
                                <p>estamos encantados de brindarte placer y confort.</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInUp animated" data-wow-delay=".5s">
                <a href="resources/images/6.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/6.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInDown animated" data-wow-delay=".5s">
                <a href="resources/images/8.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/8.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInUp animated" data-wow-delay=".5s">
                <a href="resources/images/2.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/2.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 baner-top ban-mar wow fadeInDown animated" data-wow-delay=".5s">
                <a href="resources/images/4.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/4.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 baner-top wow fadeInRight animated" data-wow-delay=".5s">
                <a href="resources/images/folder/2.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/2.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 baner-top wow fadeInLeft animated" data-wow-delay=".5s">
                <a href="resources/images/folder/4_opt.jpg" class="b-link-stripe b-animate-go  swipebox">
                    <div class="gal-spin-effect vertical ">
                        <img src="resources/images/folder/4_opt.jpg" alt=" " />
                        <div class="gal-text-box">
                            <div class="info-gal-con">
                                <h4>Hotel escuela</h4>
                                <span class="separator"></span>
                                <p>Estudiando para tu confort</p>
                                <span class="separator"></span>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //gallery -->

<!--bottom-->
<div class="bottom">
    <div class="container">
        <div class="bottom-top">
            <h3 class=" wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">Texto</h3>
            <span>Texto</span>
            <p class="wow slideInDown" data-wow-duration="1s" data-wow-delay=".5s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since, Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            <div class="start wow flipInX"  data-wow-duration="1s" data-wow-delay=".3s">
                <a href="single.html" class="hvr-bounce-to-bottom">Read More</a>
            </div>
        </div>
    </div>
</div>

<!--/contact-->
<div class="section-contact" id="contact">
    <div class="container">
        <div class="contact-main">
            <div class="col-md-6 contact-grid wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".3s">
                <h3 class="tittle wow bounceIn"  data-wow-duration=".8s" data-wow-delay=".2s">Contactanos</h3>
                <div class="arrows-three"><img src="resources/images/border.png" alt="border"></div>
                <p class="wel-text wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>
                <form id="filldetails" action="application/cos/sendMail" method="POST">
                    <div class="field name-box">
			<input type="text" id="name" placeholder="Who Are You?" required=""/>
			<label for="name">Name</label>
			<span class="ss-icon">check</span>
                    </div>
					  
		 <div class="field email-box">
                     <input type="email" id="email" placeholder="example@email.com" required=""/>
                        <label for="email">Email</label>
                        <span class="ss-icon">check</span>
		 </div>
					  
                 <div class="field phonenum-box">
                     <input type="number" id="number" placeholder="Phone Number" required=""/>
                        <label for="email">Phone</label>
                        <span class="ss-icon">check</span>
		 </div>

		 <div class="field msg-box">
                        <textarea id="msg" rows="4" placeholder="Your message goes here..." required=""/></textarea>
                        <label for="msg">Message</label>
                        <span class="ss-icon">check</span>
		  </div>
		<div class="send wow shake"  data-wow-duration="1s" data-wow-delay=".3s">
                    	<input type="submit" value="Send" >
		</div>

                </form>

            </div>
            <div class="col-md-6 contact-in wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".5s">
                <h4 class="info">Nuestra Información </h4>
                <p class="para1">Lorem ipsum dolor sit amet. Ut enim ad minim veniam.Sunt in culpa qui officia deserunt mollit anim id est laborum.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature. </p>
                <div class="con-top">
                    <h4>Alaska</h4>
                    <ul>
                        <li>4th Floor dolor sit amet,</li> 
                        <li># Grand Hyatt New York Road,</li>  
                        <li>Opp. adipiscing elit,</li> 
                        <li>Alaska - 99501</li> 
                        <li>Ph:4568956555 </li>
                        <li>Call Centre Time : 7am to 11pm</li>
                        <li><a href="mailto:info@example.com">mail@example.com</a></li>
                    </ul>
                </div>
<!--                <div class="con-top two">
                    <h4>Hawaii</h4>
                    <ul>
                        <li>4th Floor dolor sit amet,</li> 
                        <li># Grand Hyatt New York Road,</li>   
                        <li>Opp. adipiscing elit,</li> 
                        <li>Hawaii - 96815</li>
                        <li>Ph:3698521475 </li>
                        <li>Call Centre Time : 7am to 11pm</li>	
                        <li><a href="mailto:info@example.com">mail@example.com</a></li>
                    </ul>								
                </div>
            </div>-->

            <div class="clearfix"> </div>
        </div>
        <!--map-->
        <div class="map wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".5s">
           <iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d2628.879989396671!2d-100.14420293017034!3d20.18662109965365!3m2!1i1024!2i768!4f13.1!2m1!1screar+mapas+personalizados+con+google+maps!5e0!3m2!1ses!2smx!4v1468456408627" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
           
        <!--//map-->
    </div>
</div>
<!--//contact-->
<!--footer-->
<div class="footer text-center">
    <div class="container">
        <!--logo2-->
        <div class="logo2 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
            <a href="index.html"><h2>H<span>otel escuela</span></h2></a>
            <p>Estudiando para tu confort</p>
        </div>
        <!--//logo2-->
        <a href="single.html" class="flag_tag2">¿Donde nos encuentras?</a>
        <ul class="social wow slideInDown" data-wow-duration="1s" data-wow-delay=".3s">
            <li><a href="#" class="tw"></a></li>
            <li><a href="#" class="fb"> </a></li>
            <li><a href="#" class="in"> </a></li>
            <li><a href="#" class="dott"></a></li>
            <div class="clearfix"></div>
        </ul>
        <p class="copy-right wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".3s">&copy; 2016 Honest Food. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>

    </div>
</div>
<!--start-smooth-scrolling-->
<script type="text/javascript">
    $(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear' 
         };
         */

        $().UItoTop({easingType: 'easeOutQuart'});

    });
</script>
<!--end-smooth-scrolling-->
<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>